from django.urls import path, include
from projects.views import (
    project_list_view,
    project_detail_view,
    create_project_view,
)

urlpatterns = [
    path("", project_list_view, name="list_projects"),
    path("accounts/", include("accounts.urls")),
    path("<int:id>/", project_detail_view, name="show_project"),
    path("create/", create_project_view, name="create_project"),
    path("tasks/", include("tasks.urls")),
]
